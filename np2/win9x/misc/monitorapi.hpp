/**
 * @file	monitorapi.hpp
 * @breif	helper for multiple monitor apis
 */

#pragma once

#if (WINVER < 0x0500)

#if !defined(HMONITOR_DECLARED) && (WINVER < 0x0500)
#define HMONITOR_DECLARED
DECLARE_HANDLE(HMONITOR);
#endif	// !defined(HMONITOR_DECLARED)

#define MONITOR_DEFAULTTONULL		0	/*!< Returns NULL */
#define MONITOR_DEFAULTTOPRIMARY	1	/*!< Returns a handle to the primary display monitor */
#define MONITOR_DEFAULTTONEAREST	2	/*!< Returns a handle to the display monitor that is nearest to the point */

/**
 * @brief The information about a display monitor
 */
struct tagMONITORINFO
{
	DWORD cbSize;		/*!< The size of the structure, in bytes */
	RECT rcMonitor;		/*!< A RECT structure that specifies the display monitor rectangle */
	RECT rcWork;		/*!< A RECT structure that specifies the work area rectangle of the display monitor */
	DWORD dwFlags;		/*!< A set of flags that represent attributes of the display monitor */
};
typedef struct tagMONITORINFO MONITORINFO;		/*!< The information about a display monitor */
typedef struct tagMONITORINFO* LPMONITORINFO;	/*!< The pointer about a display monitor */

/**
 * The MonitorFromRect function retrieves a handle to the display monitor
 * @param[in] lprc A pointer to a RECT structure
 * @param[in] dwFlags Determines the function's return value
 * @return An HMONITOR handle to the display monitor
 */
static HMONITOR MonitorFromRect(LPCRECT lprc, DWORD dwFlags)
{
	HMODULE hUser32 = GetModuleHandle(TEXT("user32.dll"));
	if (hUser32)
	{
		typedef HMONITOR (WINAPI * FnMonitorFromRect)(LPCRECT lprc, DWORD dwFlags);
		const FnMonitorFromRect fn = reinterpret_cast<FnMonitorFromRect>(GetProcAddress(hUser32, "MonitorFromRect"));
		if (fn)
		{
			return (*fn)(lprc, dwFlags);
		}
	}
	return NULL;
}

/**
 * The MonitorFromWindow function retrieves a handle to the display monitor
 * @param[in] hwnd A handle to the window of interest
 * @param[in] dwFlags Determines the function's return value
 * @return An HMONITOR handle to the display monitor
 */
static HMONITOR MonitorFromWindow(HWND hwnd, DWORD dwFlags)
{
	HMODULE hUser32 = GetModuleHandle(TEXT("user32.dll"));
	if (hUser32)
	{
		typedef HMONITOR (WINAPI * FnMonitorFromWindow)(HWND hwnd, DWORD dwFlags);
		const FnMonitorFromWindow fn = reinterpret_cast<FnMonitorFromWindow>(GetProcAddress(hUser32, "MonitorFromWindow"));
		if (fn)
		{
			return (*fn)(hwnd, dwFlags);
		}
	}
	return NULL;
}

/**
 * Retrieves information about a display monitor
 * @param[in] hMonitor A handle to the display monitor of interest
 * @param[out] lpmi A pointer to a MONITORINFO or MONITORINFOEX structure that receives information about the specified display monitor
 * @retval nonzero If the function succeeds
 * @retval zero If the function fails
 */
static BOOL GetMonitorInfo(HMONITOR hMonitor, LPMONITORINFO lpmi)
{
	HMODULE hUser32 = GetModuleHandle(TEXT("user32.dll"));
	if (hUser32)
	{
		typedef BOOL (WINAPI * FnGetMonitorInfo)(HMONITOR hMonitor, LPMONITORINFO lpmi);
#if defined(_UNICODE)
		const FnGetMonitorInfo fn = reinterpret_cast<FnGetMonitorInfo>(GetProcAddress(hUser32, "GetMonitorInfoW"));
#else	// defined(_UNICODE)
		const FnGetMonitorInfo fn = reinterpret_cast<FnGetMonitorInfo>(GetProcAddress(hUser32, "GetMonitorInfoA"));
#endif	// defined(_UNICODE)
		if (fn)
		{
			return (*fn)(hMonitor, lpmi);
		}
	}
	return FALSE;
}

#endif	// (WINVER < 0x0500)

/**
 * ���݂̃��j�^�̈���擾����
 * @param[in] hWnd �E�B���h�E
 * @param[out] rect �̈�
 * @retval true ����
 * @retval false ���s
 */
static bool GetMonitorRect(HWND hWnd, RECT& rect)
{
	HMONITOR hMonitor = MonitorFromWindow(hWnd, MONITOR_DEFAULTTONEAREST);
	if (hMonitor != NULL)
	{
		MONITORINFO mi;
		mi.cbSize = sizeof(mi);
		if (GetMonitorInfo(hMonitor, &mi))
		{
			rect = mi.rcMonitor;
			return true;
		}
	}
	rect.left = 0;
	rect.top = 0;
	rect.right = GetSystemMetrics(SM_CXSCREEN);
	rect.bottom = GetSystemMetrics(SM_CYSCREEN);
	SystemParametersInfo(SPI_GETWORKAREA, 0, &rect, 0);
	return false;
}

/**
 * ��Ɨ̈���擾����
 * @param[in] hMonitor ���j�^
 * @param[out] rect �̈�
 * @retval true ����
 * @retval false ���s
 */
static bool GetWorkArea(HMONITOR hMonitor, RECT& rect)
{
	if (hMonitor != NULL)
	{
		MONITORINFO mi;
		mi.cbSize = sizeof(mi);
		if (GetMonitorInfo(hMonitor, &mi))
		{
			rect = mi.rcWork;
			return true;
		}
	}
	SystemParametersInfo(SPI_GETWORKAREA, 0, &rect, 0);
	return false;
}

/**
 * ���݂̍�Ɨ̈���擾����
 * @param[in] lprc �Ώۂ̗̈�
 * @param[out] rect �̈�
 * @retval true ����
 * @retval false ���s
 */
inline static bool GetWorkAreaFromRect(LPCRECT lprc, RECT& rect)
{
	return GetWorkArea(MonitorFromRect(lprc, MONITOR_DEFAULTTONEAREST), rect);
}

/**
 * ���݂̍�Ɨ̈���擾����
 * @param[in] hWnd �E�B���h�E
 * @param[out] rect �̈�
 * @retval true ����
 * @retval false ���s
 */
inline static bool GetWorkAreaFromWindow(HWND hWnd, RECT& rect)
{
	return GetWorkArea(MonitorFromWindow(hWnd, MONITOR_DEFAULTTONEAREST), rect);
}

# Neko Project II

## Overview
- [NEC](https://www.nec.com/) [PC-9801](https://ja.wikipedia.org/wiki/PC-9800%E3%82%B7%E3%83%AA%E3%83%BC%E3%82%BA) Emulator

## Reference
- WebPage: https://yui.ne.jp/np2/
- Subversion: http://amethyst.yui.ne.jp/svn/pc98/np2/trunk

## Features
- PC-9801VX21 emulation
- FM Sounds (YAMAHA YM2203, YM2608) and PCM (86PCM)
- Roland MPU-PC98II(MIDI)

## Author
- twitter: https://x.com/yuinejp
